#  Getting started

Testing and and develop kubernetes and docker using **Minikube**

 1. Install minicube, please read minicube documetation
	> https://minikube.sigs.k8s.io/docs/start/


## How to use

 1. cd to directory
 2. add **gank-global-service.yml** to cluster regristry
	> $kubectl create -fgank-global-service.yml
 3. wait until everything ready, need several minute to pull images
    >$kubectl get all
   
	![the result should be like this](https://firebasestorage.googleapis.com/v0/b/anang-event-app.appspot.com/o/Screenshot%202021-10-29%20at%209.50.45%20AM.png?alt=media&token=3438b8eb-797d-4282-b6e6-fc6953680f3e)

4. if everything ready you can try acces your service from your browser
	>$minikube service gank-global-service
5. don't forget add default path of service on your browser
	> gateway/v1/api/explorer/

	example:
	> http://127.0.0.1:61656/gateway/v1/api/explorer/

## Result

![result swegger api documentation](https://firebasestorage.googleapis.com/v0/b/anang-event-app.appspot.com/o/Screenshot%202021-10-29%20at%2010.05.19%20AM.png?alt=media&token=56f0c0a9-ff41-4c3e-b676-4151122f52d4)